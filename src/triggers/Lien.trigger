/*
	Created: RAP - December 2016 for first release
	Purpose: Auto-assign Lien Name according to standard naming convention 
*/
trigger Lien on Lien__c (before insert) {
    
    if (trigger.isBefore && trigger.isInsert) {
    	list<Id> cList = new list<Id>();
    	for (Lien__c l : trigger.new) {
    		cList.add(l.Claimant__c);
    	}
	    map<Id,Claimant__c> cMap = new map<Id,Claimant__c>([SELECT Id, Name FROM Claimant__c WHERE Id IN :cList]);
    	for (Lien__c lien : trigger.new) {
	    	lien.Name = lien.Lien_Type__c + ' - ' + cMap.get(lien.Claimant__c).Name;
	    }
    }
}