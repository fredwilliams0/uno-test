({

    doInit : function(component, event, helper) {
        var action = component.get("c.getAwards");
        action.setParams({"lId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.awards", response.getReturnValue());
            } else {
                console.log('Problem getting awards, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    }
})