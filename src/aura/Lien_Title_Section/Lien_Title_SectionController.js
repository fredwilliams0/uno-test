({

    doInit : function(component, event, helper) {
        var action = component.get("c.getLiens");
        action.setParams({"lId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.lien", response.getReturnValue());
            } else {
                console.log('Problem getting lien, response state: ' + state);
            }
        });     
        $A.enqueueAction(action);
    }
})