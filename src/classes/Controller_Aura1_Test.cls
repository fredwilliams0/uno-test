/*
	Created: RAP - January 2017 for first release
	Purpose: test function of Controller_Aura1
	Coverage as of 1/3/17 - 100% 
*/
@isTest
private class Controller_Aura1_Test {

@testSetup
	static void CreateData() {
    	Account a = new Account(Name = 'Test Account',
    							BillingStreet = '123 Main St',
    							BillingCity = 'Denver',
    							BillingState = 'CO',
    							BillingPostalCode = '80202');
    	Account b = new Account(Name = 'Payable Entity',
    							BillingStreet = '345 Elm St',
    							BillingCity = 'Portland',
    							BillingState = 'OR',
    							BillingPostalCode = '50303');
    	insert new list<Account>{a,b};
    	Contact c = new Contact(LastName = 'Crain',
    							email = 'denny@crain.com');
    	insert c;
    	Action__c action = new Action__c(Name = 'Test Action',
    									 Active__c = true,
    									 Law_Firm__c = a.Id,
    									 Law_Firm_Attorney_Contact__c = c.Id);
		insert action;
		Claimant__c claim = new Claimant__c(Address__c = '123 Main St',
											City__c = 'Denver',
											Email__c = 'rap@rap.com',
											Law_Firm__c = a.Id,
											Name = 'RAP',
											Phone__c = '3035551212',
											SSN__c = '135461448',
											State__c = 'CO',
											Zip__c = '80138');
		insert claim;
		Id aId = action.Id;
		Id cId = claim.Id;
		GeneralLedger__c gl = new GeneralLedger__c(Account__c = a.Id, 
												   Action__c = aId, 
												   Claimant__c = cId);
		insert gl;
		Lien__c lien1 = new Lien__c(Account__c = a.Id, 
									Action__c = aId, 
									Claimant__c = cId, 
									Cleared__c = false,
									Date_Submitted__c = system.today(), 
									General_Ledger__c = gl.Id, 
									Lien_Type__c = 'Private Non PLRP', 
									Notes__c = 'This is a note', 
									Payable_Entity__c = b.Id,
									Stages__c = 'Submitted', 
									State__c = 'CO', 
									Status_ERISA__c = 'MCO', 
									Submitted__c = true);
		insert lien1;
		map<integer,string> lienMap = new map<integer,string>();
		list<Lien_Negotiated_Amounts__c> lienList = new list<Lien_Negotiated_Amounts__c>();
		Date today = system.today();
		lienMap.put(0, 'Asserted');
		lienMap.put(1, 'Audit');
		lienMap.put(2, 'Neg - Proposed');
		lienMap.put(3, 'Neg - Response');
		lienMap.put(4, 'Contested');
		lienMap.put(5, 'Final');
		lienMap.put(6, 'Paid');
		for (integer i=0;i<7;i++) {
			string str = lienMap.get(i);
			Date dateStr = today.addMonths(i);
			Lien_Negotiated_Amounts__c lnac = new Lien_Negotiated_Amounts__c(Lien__c = lien1.Id, 
																			 Lien_Amount__c = 50000-(i*2500), 
																			 Lien_Amount_Date__c = dateStr, 
																			 Phase__c = lienMap.get(i));
			lienList.add(lnac);
		}
		insert lienList;
	}
    static testMethod void TestController() {
		test.startTest();
    	Lien__c l = [SELECT Id, Action__c, Claimant__c FROM Lien__c WHERE Lien_Type__c = 'Private Non PLRP' limit 1];
		ApexPages.StandardController stdctrl = new ApexPages.StandardController(l);
    	Controller_Aura1 ctrl = new Controller_Aura1(stdctrl);
		Lien__c lCheck = Controller_Aura1.getLien(l.Id);
		list<Lien__c> checkLiens = Controller_Aura1.getLiens(string.valueOf(l.Claimant__c), string.valueOf(l.Action__c)); 
		list<Injury__c> iList = Controller_Aura1.getInjuries(l.Id);
		list<Award__c> aList = Controller_Aura1.getAwards(l.Id);
		list<Claimant__c> cList = Controller_Aura1.getClaimants(string.valueOf(l.Action__c));
    	list<Lien_Negotiated_Amounts__c> l1 = Controller_Aura1.getlienAmtList(l.Id); 
    	test.stopTest();
    }
}