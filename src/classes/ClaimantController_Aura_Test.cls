/*
	Created: RAP - December 2016 for first release
	Purpose: test function of ClaimantController
	Coverage as of 12/28/16 - 96% 
*/
@isTest
public with sharing class ClaimantController_Aura_Test {
@testSetup
	public static void CreateData() {
		Id aid;
		Id cId;
    	Account a = new Account(Name = 'Test Account');
    	insert a;
    	Contact c = new Contact(LastName = 'Crain',
    							email = 'denny@crain.com');
    	insert c;
    	Action__c action = new Action__c(Name = 'Test Action',
    									 Active__c = true,
    									 Law_Firm__c = a.Id,
    									 Law_Firm_Attorney_Contact__c = c.Id);
		insert action;
		Claimant__c claim = new Claimant__c(Address__c = '123 Main St',
											City__c = 'Denver',
											Email__c = 'rap@rap.com',
											Law_Firm__c = a.Id,
											Name = 'RAP',
											Phone__c = '3035551212',
											SSN__c = '135461448',
											State__c = 'CO',
											Zip__c = '80138');
		insert claim;
		aid = action.Id;
		cId = claim.Id;
		GeneralLedger__c gl = new GeneralLedger__c(Account__c = a.Id, 
												   Action__c = aid, 
												   Claimant__c = cId);
		insert gl;
		Lien__c lien1 = new Lien__c(Account__c = a.Id, 
									Action__c = aId, 
									Claimant__c = cId, 
									Cleared__c = false,
									Date_Submitted__c = system.today(), 
									General_Ledger__c = gl.Id, 
									Lien_Type__c = 'Private Non PLRP', 
									Notes__c = 'This is a note',
									Stages__c = 'Submitted', 
									State__c = 'CO', 
									Status_ERISA__c = 'MCO', 
									Submitted__c = true);
		insert lien1;
		ICD_Code__c icd = new ICD_Code__c(Description_Long__c = 'LONG LONG LONG LONG LONG LONG LONG Description');
		insert icd;
		Injury__c injury = new Injury__c(Action__c = aId, 
										 Compensable__c = true, 
										 Claimant__c = cId, 
										 Lien__c = lien1.Id,
										 ICD_Code__c = icd.Id, 
										 Explant_Date__c = system.today().addDays(30), 
										 Implant_Date__c= system.today().addDays(-5));
		insert injury;
	}
    static testMethod void TestFail() {
		test.startTest();
    	ClaimantController_Aura ctrl = new ClaimantController_Aura();
    	try {
    		string var1 = ClaimantController_Aura.Save();
    	}
    	catch (exception e) {}
		test.stopTest();
    }

    static testMethod void TestEdit() {
    	Id aid = [SELECT Id FROM Action__c WHERE Name = 'Test Action' limit 1].Id;
    	ApexPages.currentPage().getParameters().put('aid', aid);
    	ApexPages.currentPage().getParameters().put('ssn', '135461448');
		test.startTest();
    	ClaimantController_Aura ctrl = new ClaimantController_Aura();
    	string var5 = ClaimantController_Aura.actionId;
    	var5 = ClaimantController_Aura.ssn;
    	Action__c var1 = ClaimantController_Aura.action;
    	Injury__c var2 = ClaimantController_Aura.injury;
    	Claimant__c cl = ClaimantController_Aura.claimant;
    	string var3 = ClaimantController_Aura.injuryDesc;
    	list<Lien__c> var4 = ClaimantController_Aura.liens;
    	ClaimantController_Aura.selectedLien = var4[0].Id;
    	var3 = ClaimantController_Aura.RedirectToLienDetail();
    	ClaimantController_Aura.Save();
		test.stopTest();
    }
}