/*
	Created: RAP - December 2016 for first release
	Purpose: provide context for Claimant detail page 
*/

public with sharing class ClaimantController_Aura {
//	public static final strings

//	other variables
	public static string selectedLien{get;set;}
	public static boolean editable{get;set;}
	public static list<Claimant__c> claimants{get;set;}
	
	public static string ssn {
		get {
			if (ssn == null)
				ssn = ApexPages.currentPage().getParameters().get('ssn');
			return ssn;
		}
		set;
	}
	public static string actionId {
		get {
			if (actionId == null)
				actionId = ApexPages.currentPage().getParameters().get('aid');
			return actionId;
		}
		set;
	}
	public static Claimant__c claimant {
		get {
			if (claimant == null) {
				if (!string.isBlank(ssn)) {
					claimant = [SELECT Id, Name, Address__c, City__c, Email__c, Law_Firm__c, Phone__c, SSN__c, State__c, Zip__c,
									   DOB__c, DOD__c, Gender__c, ProvidioID__c, Status__c, Law_Firm__r.Name, Law_Firm__r.Phone, 
									   Law_Firm__r.Website
								FROM Claimant__c
								WHERE SSN__c = :ssn];
					claimants = new list<Claimant__c>{claimant};	
				}
			}
			return claimant;
		}
		set;
	}
	public static Action__c action {
		get {
			if (action == null) {
				if (!string.isBlank(actionId)) {
					action = [SELECT Active__c, All_Private_Liens__c, Asserted_Private_Liens_Only__c, Case_Name__c, Fee_Structure_Plus__c, Id, 
									 Law_Firm_Attorney_Contact__c, Law_Firm_Paralegal_Contact__c, Law_Firm__c, Medicaid_Eligibility_Verification__c, 
									 Medicare_Eligibility_Verification__c, Medicare_Model__c, Medicare_Part_C__c, Medicare_Single_Event__c, 
									 Medicare_Special_Project__c, MSP_Compliance__c, MT_as_SE__c, Name, Number_of_Claimants__c, PLRP__c, Providio_Generated__c,
									 QSF_Administration__c, Salesperson1__c, Salesperson2__c, Salesperson3__c, SRP_Generated__c, Unknown_follow_up_needed__c 
							  FROM Action__c
							  WHERE Id = :actionId];
				}
			}
			return action;
		}
		set;
	}
	public static list<Injury__c> injuries {
		get {
			if (injuries == null && actionId != null) {
				injuries = [SELECT Action__c, Compensable__c, Claimant__c, Description_Long__c, Explant_Date__c, ICD_Code__c, Id, Implant_Date__c, Injury_Category__c,
								   Injury_Description__c, Last_Treatment_Date__c, Lien__c, Name, Non_Surgical_Treatment_Enhancer__c, Non_Surgical_Treatment__c,
								   Surgical_Facility__c, Surgical_Treatment_Enhancer__c, Total_Surgery_Per_Defendant__c, Treatment_Enhancer__c, Base_Category__c,
								   Settlement_Category__c
							FROM Injury__c
							WHERE Claimant__c = :claimant.Id
							AND Action__c = :actionId];	
			}
			return injuries;
		}
		set;
	}
	public static Injury__c injury {
		get {
			if (injuries != null && !injuries.isEmpty())
				injury = injuries[0];
			return injury;
		}
		set;
	}
	public static string injuryDesc {
		get {
			if (injuryDesc == null) 
				injuryDesc = injury.Description_Long__c;
			return injuryDesc;
		}
		set;
	}
	public static list<Lien__c> liens {
		get {
			if (liens == null) {
				liens = [SELECT Account__c, Action__c, Claimant__c, Cleared__c, Date_No_Interest__c, Date_Payment__c,
							    Date_Re_sweep_Sent__c, Date_Submitted__c, Deal_Made__c, Eligible__c, Final_Demand_Amount__c,
							    Final_Demand_Received__c, Final_Lien_Amt_Paid__c, General_Ledger__c, Health_Plan__c, HICN__c,
							    Id, LienAmount__c, Lien_Holder_File_ID__c, Lien_Type__c, Name, Notes__c, Payable_Entity__c,
							    Policy__c, Re_sweep_Sent__c, Stages__c, State__c, Status_ERISA__c, Status__c, Submitted__c,
							    Subro_Rep__c
						 FROM Lien__c
						 WHERE Claimant__c = :claimant.Id
						 AND Action__c = :actionId];	
			}
			return liens;
		}
		set;
	}
	
// constructor
	public ClaimantController_Aura() {
		if (string.isBlank(ApexPages.currentPage().getParameters().get('aid')) ||
			string.isBlank(ApexPages.currentPage().getParameters().get('ssn'))) {
			system.debug(loggingLevel.INFO, 'RAP --->> aid = ' + ApexPages.currentPage().getParameters().get('aid'));
			system.debug(loggingLevel.INFO, 'RAP --->> ssn = ' + ApexPages.currentPage().getParameters().get('ssn'));
			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Missing URL parameters. Please contact your system administrator.'));
		}
	}
	
// action methods
@auraEnabled	
	public static string Save() {
		if (!Validate())
			return 'Error';
		database.saveResult sr;
		try {
			sr = database.update(claimant);
			editable = false;
		}
		catch (exception e) {
			throw new AuraHandledException('Exception caught saving Claimant: ' + e.getMessage());
		}
		if (sr != null && !sr.isSuccess()) {
			throw new AuraHandledException('Claimant save failed: ' + sr.getErrors());
		}
		return 'Success';
	}
	
	private static boolean Validate() {
		boolean isValid = true;
		return isValid;
	}
	
@auraEnabled
	public static string RedirectToLienDetail() {
		return 'apex/LienDetail?id=' + selectedLien;
	}
}